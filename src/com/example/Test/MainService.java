package com.example.Test;

import android.app.Service;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import com.autoupdateapk.AutoUpdateApk;
import com.autoupdateapk.SilentAutoUpdate;
import com.couchbase.android.CouchbaseMobile;
import com.couchbase.android.ICouchbaseDelegate;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.android.http.AndroidHttpClient;
import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.net.MalformedURLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MainService extends Service {
    private ServiceConnection couchServiceConnection;
    private transient boolean running = true;
    private MainThread mainThread;
    private HttpClient authenticatedHttpClient;
    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
    private CouchDbConnector db;
    private SilentAutoUpdate silentAutoUpdate;

    private final static String TAG = "Test MainService";


    private class MainThread extends Thread {

        public MainThread() {
            super("MainServiceThread");
        }

        @Override
        public void run() {
            while (running) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        scheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);

        scheduledThreadPoolExecutor.schedule(
                new Runnable() {
                    @Override
                    public void run() {
                        startCouchbase();
                    }
                }, 2, TimeUnit.SECONDS);

        mainThread = new MainThread();
        mainThread.start();

        //lance la connection a la DB
        scheduledThreadPoolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    authenticatedHttpClient = new AndroidHttpClient.Builder()
                            .url("http://127.0.0.1:5984")
                            .build();
                    CouchDbInstance dbInstance = new StdCouchDbInstance(authenticatedHttpClient);
                    db = new StdCouchDbConnector("test", dbInstance);
                    db.createDatabaseIfNotExists();
                    db.compact();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });

        //lance le service de Mise a jour
        silentAutoUpdate = new SilentAutoUpdate(getApplicationContext(), "/apkupdate", "http://192.168.1.100:8123");
        silentAutoUpdate.setUpdateInterval(1 * AutoUpdateApk.DAYS);

        scheduledThreadPoolExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                silentAutoUpdate.checkUpdatesManually();
            }
        }, 1, 30, TimeUnit.MINUTES);

        // compactage de la DB interne toute les 2 heures
        scheduledThreadPoolExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (db != null) {
                    db.compact();
                }
            }
        }, 2, 2, TimeUnit.HOURS);


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        running = false;
        try {
            mainThread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }


    private final ICouchbaseDelegate mDelegate = new ICouchbaseDelegate() {
        @Override
        public void couchbaseStarted(String host, int port) {
            Log.v(TAG, "started couchbase - " + host + ":" + port);
        }

        @Override
        public void exit(String error) {
            Log.w(TAG, "exiting couchbase - " + error);
        }
    };

    public void startCouchbase() {
        CouchbaseMobile couch = new CouchbaseMobile(getBaseContext(), mDelegate);
        couchServiceConnection = couch.startCouchbase();
    }
}
